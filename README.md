# Recettes

[![CodeFactor](https://www.codefactor.io/repository/github/ewenquim/recettes/badge)](https://www.codefactor.io/repository/github/ewenquim/recettes)
[![GitHub Actions](https://img.shields.io/endpoint.svg?url=https%3A%2F%2Factions-badge.atrox.dev%2Fatrox%2Fsync-dotenv%2Fbadge&label=build&logo=none)](https://actions-badge.atrox.dev/EwenQuim/recettes/goto)

A website giving you recipe ideas for an healthy week.

Tired of unique random recipes? Try to get weekly equilibrium!
